# Portfolio Project V2

## Description
This is a second iteration of a personal portfolio website that is aimed to improve the previous version, with features such as easier modification, readable files and code, structured and flexible design, and other refactoring improvements.